package com.example.bankapp.dto;

import lombok.Data;

@Data
public class UserDto {
    private String userName;
}
