package com.example.bankapp.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "card_benefits")
@Data
public class CardBenefit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;
    @ManyToOne
    private Card card;
}