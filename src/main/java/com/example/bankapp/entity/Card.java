package com.example.bankapp.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity
@Data
@Table(name = "cards")
public class Card {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Account account;

    private String cardNumber;
    private String cardType;
    private String expirationDate;

    @OneToMany(mappedBy = "card", cascade = CascadeType.ALL)
    private List<CardBenefit> benefits;
}